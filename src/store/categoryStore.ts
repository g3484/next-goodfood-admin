import create from 'zustand';
import { getCategories } from '../services/axios/categoryRequest';

interface CategoryStore {
  categories: [
    {
      id: string;
      name: string;
    }
  ];

  setCategories: () => Promise<{
    data: any;
    status: number;
  }>;
}

const useCategoryStore = create<CategoryStore>((set) => ({
  categories: [
    {
      id: '',
      name: '',
    },
  ],

  setCategories: async () => {
    const res = await getCategories('/api/products/category');

    if (res.status === 200) {
      set(() => ({
        categories: res.data,
      }));
    }

    return res;
  },
}));

export default useCategoryStore;
