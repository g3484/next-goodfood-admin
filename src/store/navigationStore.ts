import create from 'zustand';

interface navigationItem {
  navigation: string;
  setNavigation: (navigation: string) => void;
  resetNavigation: () => void;
}

const useNavigationStore = create<navigationItem>((set) => ({
  navigation: 'products',

  setNavigation: (slug) =>
    set(() => ({
      navigation: slug,
    })),

  resetNavigation: () =>
    set(() => ({
      navigation: 'reclamation',
    })),
}));

export default useNavigationStore;
