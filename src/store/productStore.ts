import create from 'zustand';
import { getProducts } from '../services/axios/productRequest';

interface ProductStore {
  products: [
    {
      id: string;
      name: string;
      price: number;
      description: string;
      imageUrl: string;
    }
  ];
  setProducts: () => Promise<{
    data: any;
    status: number;
  }>;
}

const useProductStore = create<ProductStore>((set) => ({
  products: [
    {
      id: '',
      name: '',
      price: 0,
      description: '',
      imageUrl: '',
    },
  ],

  setProducts: async () => {
    const res = await getProducts('/api/products');

    if (res.status === 200) {
      set(() => ({
        products: res.data,
      }));
    }

    return res;
  },
}));

export default useProductStore;
