import create from 'zustand';
import { toast, ToastOptions } from 'react-toastify';

interface toastNotificationItem {
  emitNotification: (message: string, type: string) => void;
}

const config = {
  theme: 'colored',
  position: 'top-right',
  autoClose: 5000,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
} as ToastOptions;

const useToastNavigationStore = create<toastNotificationItem>((set) => ({
  emitNotification: (message, type) => {
    if (type === 'error') {
      toast.error(message, {
        ...config,
      });
    }

    if (type === 'success') {
      toast.success(message, {
        ...config,
      });
    }
  },
}));

export default useToastNavigationStore;
