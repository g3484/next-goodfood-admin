import create from 'zustand';

interface userItem {
  id: string;
  lastname: string;
  firstname: string;
  mailAddress: string;
  roles: string[];
}

const useCurrentUserStore = create<{
  user: userItem;
  setCurrentUser: (user: userItem) => void;
}>((set, get) => ({
  user: {
    id: '',
    lastname: '',
    firstname: '',
    mailAddress: '',
    roles: [],
  },

  setCurrentUser: (user: userItem) => set({ user }),
}));

export default useCurrentUserStore;
