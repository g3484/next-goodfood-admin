import React, { useRef, useState } from 'react';
import Spinner from '../../components/Spinner/Spinner';
import TableHeader from '../../components/TableHeader/TableHeader';
import { createCategory } from '../../services/axios/categoryRequest';
import styles from './AddCategory.module.scss';
import useToastNavigationStore from '../../store/toastNotificationStore';
import { useRouter } from 'next/router';
import useNavigationStore from '../../store/navigationStore';

const AddCategory = () => {
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const nameRef = useRef(null);

  const { emitNotification } = useToastNavigationStore();
  const { setNavigation } = useNavigationStore();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    const name = nameRef.current.value;

    const { status } = await createCategory('/api/products/category', { name });

    if (status === 201) {
      emitNotification('Catégorie ajoutée', 'success');
      setLoading(false);
      setNavigation('category');
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 201) {
      emitNotification('Une erreur est survenue', 'error');
    }
  };

  return (
    <div className={styles.AddCategory}>
      {loading ? (
        <Spinner loading={loading} color="#0B70B5" />
      ) : (
        <>
          <TableHeader icn="addcat">
            <p className={styles.AddCategory__header}>Ajouter une categorie</p>
          </TableHeader>
          <div className={styles.AddCategory__content}>
            <form
              className={styles.AddCategory__content__infos}
              onSubmit={handleSubmit}
            >
              <div>
                <p>Nom de la categorie</p>
                <input type="text" ref={nameRef} required />
              </div>

              <button type="submit">Ajouter</button>
            </form>
          </div>
        </>
      )}
    </div>
  );
};

export default AddCategory;
