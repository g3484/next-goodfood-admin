import React, { useEffect, useState } from 'react';
import styles from './Category.module.scss';
import Spinner from '../../components/Spinner/Spinner';
import { useRouter } from 'next/router';
import useToastNavigationStore from '../../store/toastNotificationStore';
import TableHeader from '../../components/TableHeader/TableHeader';
import CategoryDetails from './CategoryDetails/CategoryDetails';
import useCategoryStore from '../../store/categoryStore';

const Category = () => {
  const [loading, setLoading] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const { emitNotification } = useToastNavigationStore();
  const router = useRouter();
  const { categories, setCategories } = useCategoryStore();

  const fetchCategories = async () => {
    setLoading(true);

    const { status } = await setCategories();

    if (status === 200) {
      setLoading(false);
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      emitNotification('Une erreur est survenue', 'error');
    }
  };

  useEffect(() => {
    fetchCategories();
  }, []);

  return (
    <div className={styles.Category}>
      {loading ? (
        <Spinner loading={loading} color="#0B70B5" />
      ) : (
        <>
          {!selectedCategory ? (
            <>
              <TableHeader icn="cat">
                <p className={styles.Category__header}>Liste des categories</p>
              </TableHeader>

              <div className={styles.Category__list}>
                {categories.map((el) => (
                  <div
                    className={styles.Category__list__item}
                    key={el.id}
                    onClick={() => setSelectedCategory(el.id)}
                  >
                    <p className={styles.Category__list__item__title}>
                      {el.name}
                    </p>
                  </div>
                ))}
              </div>
            </>
          ) : (
            <CategoryDetails
              categoryId={selectedCategory}
              close={() => setSelectedCategory(null)}
            />
          )}
        </>
      )}
    </div>
  );
};

export default Category;
