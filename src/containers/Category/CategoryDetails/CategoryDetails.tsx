/* eslint-disable indent */
import React, { useState, useRef, useEffect } from 'react';
import styles from './CategoryDetails.module.scss';
import TableHeader from '../../../components/TableHeader/TableHeader';
import Spinner from '../../../components/Spinner/Spinner';
import { useRouter } from 'next/router';
import useToastNavigationStore from '../../../store/toastNotificationStore';
import {
  deleteCategory,
  getCategory,
  updateCategory,
} from '../../../services/axios/categoryRequest';
import useCategoryStore from '../../../store/categoryStore';

type CategoryDetailssProps = {
  categoryId: string;
  close: () => void;
};

const CategoryDetails = ({ categoryId, close }: CategoryDetailssProps) => {
  const [loading, setLoading] = useState(true);
  const [category, setCategory] = useState(null);
  const { emitNotification } = useToastNavigationStore();
  const nameRef = useRef(null);

  const router = useRouter();

  const { setCategories } = useCategoryStore();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    const name = nameRef.current.value;

    const { status } = await updateCategory(
      `/api/products/category/${categoryId}`,
      {
        name,
      }
    );

    if (status === 200) {
      emitNotification('Catégorie de produit modifiée avec succès', 'success');

      const { status } = await setCategories();

      if (status === 200) {
        setLoading(false);
        close();
      }

      if (status === 401) {
        return router.push('/login');
      }

      if (status !== 200) {
        emitNotification('Une erreur est survenue', 'error');
      }
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      emitNotification(
        'Une erreur est survenue lors de la modification du produit',
        'error'
      );
    }
  };

  const handleDelete = async () => {
    setLoading(true);

    const { status } = await deleteCategory(
      `/api/products/category/${categoryId}`
    );

    if (status === 200) {
      emitNotification('Catégorie de produit supprimée avec succès', 'success');

      const { status } = await setCategories();

      if (status === 200) {
        setLoading(false);
        close();
      }

      if (status === 401) {
        return router.push('/login');
      }

      if (status !== 200) {
        emitNotification('Une erreur est survenue', 'error');
      }
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      emitNotification(
        'Une erreur est survenue lors de la suppression du produit',
        'error'
      );
    }
  };

  const fetchCategory = async () => {
    setLoading(true);
    const { data, status } = await getCategory(
      `/api/products/category/${categoryId}`
    );

    if (status === 200) {
      console.log(data);
      setCategory(data);
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      emitNotification(
        'Une erreur est survenue lors de la récupération du produit',
        'error'
      );
    }
  };

  useEffect(() => {
    fetchCategory();
  }, []);

  useEffect(() => {
    if (loading) return;

    nameRef.current.value = category.name;
  }, [loading]);

  useEffect(() => {
    if (!category) return;

    console.log(category, 'cat');

    setLoading(false);
  }, [category]);

  return (
    <div className={styles.CategoryDetails}>
      {loading ? (
        <Spinner loading={loading} color="#0B70B5" />
      ) : (
        <>
          <TableHeader icn="back" onClick={close}>
            <p className={styles.CategoryDetails__header}>Retour à la liste</p>
          </TableHeader>

          {category && (
            <div className={styles.CategoryDetails__content}>
              <form
                className={styles.CategoryDetails__content__infos}
                onSubmit={handleSubmit}
              >
                <div>
                  <p>Nom de la categorie</p>
                  <input type="text" ref={nameRef} required />
                </div>

                <button type="submit">Modifier</button>
              </form>

              <div
                className={styles.CategoryDetails__content__delete}
                onClick={handleDelete}
              >
                <img src="svg/icn-bin.svg" alt="" />
              </div>
            </div>
          )}
        </>
      )}
    </div>
  );
};

export default CategoryDetails;
