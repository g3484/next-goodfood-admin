import React, { useState } from 'react';
import styles from './Content.module.scss';
import Window from '../../components/Window/Window';
import MenuIcon from '../../components/MenuIcon/MenuIcon';
import useNavigationStore from '../../store/navigationStore';
import { useRouter } from 'next/router';
import Products from '../Products/Products';
import Category from '../Category/Category';
import AddProduct from '../AddProduct/AddProduct';
import AddCategory from '../AddCategory/AddCategory';
import Roles from '../Roles/Roles';

const Content = () => {
  const { navigation } = useNavigationStore();
  const [showLogoutPopup, setShowLogoutPopup] = useState(false);
  const router = useRouter();

  const disconnect = async () => {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');

    return router.push('/auth/signin');
  };

  return (
    <div className={styles.Content}>
      <div className={styles.Content__menu}>
        <MenuIcon
          slug="products"
          icn="product"
          active={navigation === 'products'}
        />
        <MenuIcon
          slug="addProduct"
          icn="addproduct"
          active={navigation === 'addProduct'}
        />
        <MenuIcon
          slug="category"
          icn="cat"
          active={navigation === 'category'}
        />
        <MenuIcon
          slug="addCategory"
          icn="addcat"
          active={navigation === 'addCategory'}
        />
        <MenuIcon slug="roles" icn="mask" active={navigation === 'roles'} />

        <MenuIcon
          icn="logout"
          hasOnClick={true}
          onClick={() => setShowLogoutPopup(true)}
        />
      </div>

      <Window>
        {navigation === 'products' && <Products />}
        {navigation === 'addProduct' && <AddProduct />}
        {navigation === 'category' && <Category />}
        {navigation === 'addCategory' && <AddCategory />}
        {navigation === 'roles' && <Roles />}
      </Window>

      {showLogoutPopup && (
        <div className={styles.Content__logout}>
          <div
            className={styles.Content__logout__layer}
            onClick={() => setShowLogoutPopup(false)}
          ></div>
          <div className={styles.Content__logout__modal}>
            <p>Confirmez-vous la déconnexion ?</p>
            <div className={styles.Content__logout__modal__btns}>
              <button
                className={styles.Content__logout__modal__btns__accept}
                onClick={disconnect}
              >
                Confirmer
              </button>
              <button
                className={styles.Content__logout__modal__btns__deny}
                onClick={() => setShowLogoutPopup(false)}
              >
                Annuler
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Content;
