/* eslint-disable indent */
import React, { useState, useRef, useEffect } from 'react';
import styles from './ProductDetails.module.scss';
import TableHeader from '../../../components/TableHeader/TableHeader';
import Spinner from '../../../components/Spinner/Spinner';
import { useRouter } from 'next/router';
import useToastNavigationStore from '../../../store/toastNotificationStore';
import {
  getProduct,
  deleteProduct,
} from '../../../services/axios/productRequest';
import { getCategories } from '../../../services/axios/categoryRequest';
import { updateProduct } from '../../../services/axios/productRequest';
import useProductStore from '../../../store/productStore';

interface ProductDetailsProps {
  productId: string;
  close: () => void;
}

const ProductDetails = ({ productId, close }: ProductDetailsProps) => {
  const [loading, setLoading] = useState(true);
  const [product, setProduct] = useState(null);
  const [categories, setCategories] = useState([]);
  const { emitNotification } = useToastNavigationStore();
  const imageRef = useRef(null);
  const fileRef = useRef(null);
  const nameRef = useRef(null);
  const priceRef = useRef(null);
  const descriptionRef = useRef(null);
  const categoryRef = useRef(null);

  const { setProducts } = useProductStore();

  const router = useRouter();

  useEffect(() => {
    if (loading) return;

    nameRef.current.value = product.name;
    descriptionRef.current.value = product.description;
    priceRef.current.value = product.price;
    imageRef.current.src = product.imageUrl;
    categoryRef.current.value = product.idCategory;
  }, [loading]);

  useEffect(() => {
    if (!product || !categories) return;

    setLoading(false);
  }, [product, categories]);

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onloadend = () => {
      imageRef.current.src = reader.result;
    };
    reader.readAsDataURL(file);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    const name = nameRef.current.value;
    const description = descriptionRef.current.value;
    const price = priceRef.current.value;
    const idCategory = categoryRef.current.value;
    const image = fileRef.current.files[0];
    const id = productId;

    console.log(idCategory, 'idcat');

    const formData = new FormData();
    formData.append('name', name);
    formData.append('description', description);
    formData.append('price', price);
    formData.append('idCategory', idCategory);
    if (image) formData.append('image', image);

    const { status } = await updateProduct(`/api/products/${id}`, formData);

    if (status === 200) {
      emitNotification('Produit modifié avec succès', 'success');

      const { status } = await setProducts();

      if (status === 200) {
        setLoading(false);
        close();
      }

      if (status === 401) {
        router.push('/login');
      }

      if (status !== 200) {
        emitNotification('Une erreur est survenue', 'error');
      }
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      emitNotification(
        'Une erreur est survenue lors de la modification du produit',
        'error'
      );
    }
  };

  const handleDelete = async () => {
    setLoading(true);
    const { status } = await deleteProduct(`/api/products/${productId}`);

    if (status === 200) {
      emitNotification('Produit supprimé avec succès', 'success');

      const { status } = await setProducts();

      if (status === 200) {
        setLoading(false);
        close();
      }

      if (status === 401) {
        router.push('/login');
      }

      if (status !== 200) {
        emitNotification('Une erreur est survenue', 'error');
      }
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      emitNotification(
        'Une erreur est survenue lors de la suppression du produit',
        'error'
      );
    }
  };

  const fetchProduct = async () => {
    setLoading(true);
    const { data, status } = await getProduct(`/api/products/${productId}`);

    if (status === 200) {
      setProduct(data);
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      emitNotification(
        'Une erreur est survenue lors de la récupération du produit',
        'error'
      );
    }
  };

  const fetchCategories = async () => {
    setLoading(true);
    const { data, status } = await getCategories(`/api/products/category`);

    if (status === 200) {
      setCategories(data);
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      emitNotification(
        'Une erreur est survenue lors de la récupération du produit',
        'error'
      );
    }
  };

  useEffect(() => {
    fetchProduct();
    fetchCategories();
  }, []);

  return (
    <div className={styles.ProductDetails}>
      {loading ? (
        <Spinner loading={loading} color="#0B70B5" />
      ) : (
        <>
          <TableHeader icn="back" onClick={close}>
            <p className={styles.ProductDetails__header}>Retour à la liste</p>
          </TableHeader>

          <div className={styles.ProductDetails__content}>
            <form onSubmit={handleSubmit}>
              <div className={styles.ProductDetails__content__image}>
                <img ref={imageRef} alt={product?.name} />
                <input type="file" ref={fileRef} onChange={handleImageChange} />
              </div>
              <div
                className={styles.ProductDetails__content__infos}
                onSubmit={handleSubmit}
              >
                <div>
                  <p>Nom du produit</p>
                  <input type="text" ref={nameRef} required />
                </div>

                <div>
                  <p>Description</p>
                  <input type="text" ref={descriptionRef} required />
                </div>

                <div>
                  <p>Prix</p>
                  <input type="text" ref={priceRef} required />
                </div>

                <div>
                  <p>Categorie</p>
                  <select
                    ref={categoryRef}
                    defaultValue={product?.idCategory}
                    required
                  >
                    {categories.map((c) => (
                      <option
                        value={c.id}
                        key={c.id}
                        selected={c.id === product?.idCategory}
                      >
                        {c.name}
                      </option>
                    ))}
                  </select>
                </div>

                <button type="submit">Modifier</button>
              </div>
            </form>

            <div
              className={styles.ProductDetails__content__delete}
              onClick={handleDelete}
            >
              <img src="svg/icn-bin.svg" alt="" />
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default ProductDetails;
