import React, { useEffect, useState } from 'react';
import styles from './Products.module.scss';
import Spinner from '../../components/Spinner/Spinner';
import { useRouter } from 'next/router';
import useToastNavigationStore from '../../store/toastNotificationStore';
import TableHeader from '../../components/TableHeader/TableHeader';
import ProductDetails from './ProductDetails/ProductDetails';
import useProductStore from '../../store/productStore';

const Products = () => {
  const [loading, setLoading] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const { emitNotification } = useToastNavigationStore();
  const router = useRouter();

  const { products, setProducts } = useProductStore();

  const fetchProducts = async () => {
    setLoading(true);
    const { status } = await setProducts();

    if (status === 200) {
      setLoading(false);
    }

    if (status === 401) {
      router.push('/login');
    }

    if (status !== 200) {
      emitNotification('Une erreur est survenue', 'error');
    }
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div className={styles.Products}>
      {loading ? (
        <Spinner loading={loading} color="#0B70B5" />
      ) : (
        <>
          {!selectedProduct ? (
            <>
              <TableHeader icn="product">
                <p className={styles.Products__header}>Liste des produits</p>
              </TableHeader>

              <div className={styles.Products__list}>
                {products.map((el) => (
                  <div
                    className={styles.Products__list__item}
                    key={el.id}
                    onClick={() => setSelectedProduct(el.id)}
                  >
                    <img src={el.imageUrl} alt="" />
                    <p className={styles.Products__list__item__title}>
                      {el.name}
                    </p>
                    <p className={styles.Products__list__item__description}>
                      {el.description}
                    </p>
                    <p className={styles.Products__list__item__price}>
                      {el.price} €
                    </p>
                  </div>
                ))}
              </div>
            </>
          ) : (
            <ProductDetails
              productId={selectedProduct}
              close={() => setSelectedProduct(null)}
            />
          )}
        </>
      )}
    </div>
  );
};

export default Products;
