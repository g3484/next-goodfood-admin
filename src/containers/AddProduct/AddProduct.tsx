import React, { useEffect, useRef, useState } from 'react';
import Spinner from '../../components/Spinner/Spinner';
import TableHeader from '../../components/TableHeader/TableHeader';
import styles from './AddProduct.module.scss';
import { createProduct } from '../../services/axios/productRequest';
import { getCategories } from '../../services/axios/categoryRequest';
import { useRouter } from 'next/router';
import useToastNavigationStore from '../../store/toastNotificationStore';
import useNavigationStore from '../../store/navigationStore';

const AddProduct = () => {
  const [loading, setLoading] = useState(false);
  const [categories, setCategories] = useState([]);

  const imageRef = useRef(null);
  const fileRef = useRef(null);
  const nameRef = useRef(null);
  const priceRef = useRef(null);
  const descriptionRef = useRef(null);
  const categoryRef = useRef(null);

  const { emitNotification } = useToastNavigationStore();
  const { setNavigation } = useNavigationStore();

  const router = useRouter();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    const name = nameRef.current.value;
    const description = descriptionRef.current.value;
    const price = priceRef.current.value;
    const idCategory = categoryRef.current.value;
    const file = fileRef.current.files[0];

    const formData = new FormData();
    formData.append('name', name);
    formData.append('description', description);
    formData.append('price', price);
    formData.append('idCategory', idCategory);
    formData.append('image', file);

    const { status } = await createProduct('/api/products', formData);

    if (status === 201) {
      emitNotification('Produit ajouté avec succès', 'success');
      setLoading(false);
      setNavigation('products');
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 201) {
      emitNotification('Une erreur est survenue', 'error');
    }
  };

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onloadend = () => {
      imageRef.current.src = reader.result;
    };
    reader.readAsDataURL(file);
  };

  const fetchCategories = async () => {
    setLoading(true);
    const { data, status } = await getCategories(`/api/products/category`);

    if (status === 200) {
      setCategories(data);
      setLoading(false);
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      emitNotification(
        'Une erreur est survenue lors de la récupération du produit',
        'error'
      );
    }
  };

  useEffect(() => {
    fetchCategories();
  }, []);

  return (
    <div className={styles.AddProduct}>
      {loading ? (
        <Spinner loading={loading} color="#0B70B5" />
      ) : (
        <>
          <TableHeader icn="addproduct">
            <p className={styles.AddProduct__header}>Ajouter un produit</p>
          </TableHeader>
          <div className={styles.AddProduct__content}>
            <div className={styles.AddProduct__content__image}>
              <img ref={imageRef} />
              <input
                type="file"
                ref={fileRef}
                onChange={handleImageChange}
                required
              />
            </div>
            <form
              className={styles.AddProduct__content__infos}
              onSubmit={handleSubmit}
            >
              <div>
                <p>Nom du produit</p>
                <input type="text" ref={nameRef} required />
              </div>

              <div>
                <p>Description</p>
                <input type="text" ref={descriptionRef} required />
              </div>

              <div>
                <p>Prix</p>
                <input type="text" ref={priceRef} required />
              </div>

              <div>
                <p>Categorie</p>
                <select ref={categoryRef} required>
                  <option disabled selected>
                    Selectionner une categorie
                  </option>
                  {categories.map((c) => (
                    <option value={c.id} key={c.id}>
                      {c.name}
                    </option>
                  ))}
                </select>
              </div>

              <button type="submit">Ajouter</button>
            </form>
          </div>
        </>
      )}
    </div>
  );
};

export default AddProduct;
