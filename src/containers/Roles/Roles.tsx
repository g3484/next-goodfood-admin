import React, { useEffect, useState } from 'react';
import Spinner from '../../components/Spinner/Spinner';
import TableHeader from '../../components/TableHeader/TableHeader';
import { getRoles, getUsers } from '../../services/axios/userRequest';
import styles from './Roles.module.scss';
import { useRouter } from 'next/router';

const Roles = () => {
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState([]);
  const [selected, setSelected] = useState(null);
  const [roles, setRoles] = useState([]);

  const router = useRouter();

  const fetchUsers = async () => {
    setLoading(true);
    const { data, status } = await getUsers('/api/users/admin/users');

    if (status === 200) {
      console.log(data);
      setUsers(data);
    }

    if (status === 401) {
      router.push('/login');
    }

    if (status !== 200) {
      console.log(data);
      alert('Une erreur est survenue');
    }
  };

  const fetchRoles = async () => {
    setLoading(true);
    const { data, status } = await getRoles('/api/users/admin/roles/all');

    if (status === 200) {
      console.log(data);
      setRoles(data);
    }

    if (status === 401) {
      router.push('/login');
    }

    if (status !== 200) {
      console.log(data);
      alert('Une erreur est survenue');
    }
  };

  const updateUserRoles = async () => {};

  const handleCheckbox = (selected, role) => {
    if (!selected) return;
    const user = users.find((user) => user.id === selected);

    console.log(user, 'user');
    const hasRole = user.roles.find((userRole) => {
      if (
        userRole.trim() === 'ROLE_DELIVERER' &&
        role.value.trim() === 'ROLE_DELIVERY'
      ) {
        return true;
      }
      return userRole.trim() === role.value.trim();
    });

    console.log(hasRole, 'hasRole');

    return hasRole ? true : false;
  };

  useEffect(() => {
    fetchUsers();
    fetchRoles();
  }, []);

  useEffect(() => {
    if (!users.length && !roles.length) return;
    setLoading(false);
  }, [users, roles]);

  return (
    <div className={styles.Roles}>
      {loading ? (
        <Spinner loading={loading} color="#0B70B5" />
      ) : (
        <>
          <TableHeader icn="mask">
            <p className={styles.Roles__header}>Modification du role</p>
          </TableHeader>
          <div className={styles.Roles__content}>
            <div className={styles.Roles__content__users}>
              {users.map((user) => (
                <div
                  className={styles.Roles__content__users__item}
                  key={user.id}
                  onClick={() => setSelected(user.id)}
                >
                  <input
                    type="radio"
                    name="user"
                    className={styles.Roles__content__users__item__checkbox}
                    checked={selected === user.id}
                  />

                  <p className={styles.Roles__content__users__item__firstname}>
                    {user.firstname}
                  </p>
                  <p className={styles.Roles__content__users__item__lastname}>
                    {user.lastname}
                  </p>
                  <p className={styles.Roles__content__users__item__mail}>
                    {user.mailAddress}
                  </p>
                </div>
              ))}
            </div>

            <div className={styles.Roles__content__roles}>
              {roles.map((role) => {
                const hasRole = handleCheckbox(selected, role);

                return (
                  <div
                    className={styles.Roles__content__roles__item}
                    key={role.id}
                  >
                    <input
                      type="checkbox"
                      className={styles.Roles__content__roles__item__checkbox}
                      defaultChecked={hasRole}
                    />
                    <p className={styles.Roles__content__roles__item__name}>
                      {role.name}
                    </p>
                  </div>
                );
              })}
            </div>

            <button
              className={styles.Roles__content__btn}
              onClick={updateUserRoles}
            >
              Ajouter
            </button>
          </div>
        </>
      )}
    </div>
  );
};

export default Roles;
