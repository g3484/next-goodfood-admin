import React from 'react';
import styles from './LoginContainer.module.scss';
import Form from '../../components/form/form';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import IFormLogin from '../../interfaces/login/IFormLogin.interface';
import loginSchema from '../../validation/login/login.schema';
import { inputLoginData } from '../../data/login/login.data';
import { login } from '../../services/axios/authRequest';
import { useRouter } from 'next/router';

import useCurrentUserStore from '../../store/currentUserStore';

const LoginContainer = () => {
  const router = useRouter();
  const { setCurrentUser } = useCurrentUserStore();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormLogin>({
    resolver: yupResolver(loginSchema),
  });

  const trimInput = (input: string) => {
    return input.trim();
  };

  const onSubmit = async (formData: IFormLogin) => {
    const { email, password } = formData;

    const mailAddress = email;

    const {
      data,
      status,
    }: {
      data: {
        jwt: string;
        refreshToken: string;
        user: {
          id: string;
          lastname: string;
          firstname: string;
          mailAddress: string;
          roles: string[];
        };
      };
      status: number;
    } = await login('/api/users/login', { mailAddress, password });

    if (status === 200) {
      const { jwt, refreshToken, user } = data;

      setCurrentUser({
        id: trimInput(user.id),
        lastname: trimInput(user.lastname),
        firstname: trimInput(user.firstname),
        mailAddress: trimInput(user.mailAddress),
        roles: user.roles.map((role) => trimInput(role)),
      });

      localStorage.setItem('access_token', jwt);
      localStorage.setItem('refresh_token', refreshToken);

      router.push('/');
    }

    if (status === 401) {
      console.log('wrong credentials');
    }

    if (status !== 200) {
      alert('error');
    }
  };

  return (
    <div className={styles.LoginContainer}>
      <h1>Connexion</h1>
      <Form
        inputs={inputLoginData}
        btnLabel={'Connexion'}
        register={register}
        handleSubmit={handleSubmit}
        onSubmit={onSubmit}
        errors={errors}
        inputsClassName={styles.LoginContainer__inputs}
        btnClassName={styles.LoginContainer__btn}
      />
    </div>
  );
};

export default LoginContainer;
