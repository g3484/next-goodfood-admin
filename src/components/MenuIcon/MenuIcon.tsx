import React from 'react';
import styles from './MenuIcon.module.scss';
import useNavigationStore from '../../store/navigationStore';

type MenuIconProps = {
  slug?: string;
  icn: string;
  active?: boolean;
  hasOnClick?: boolean;
  onClick?: () => void;
};

const MenuIcon = ({
  slug = '',
  icn,
  active = false,
  hasOnClick = false,
  onClick = () => {},
}: MenuIconProps) => {
  const setNavigation = useNavigationStore((state) => state.setNavigation);

  return (
    <div
      className={`${styles.MenuIcon} ${active && styles.MenuIcon__active}`}
      onClick={hasOnClick ? onClick : () => setNavigation(slug)}
    >
      <img src={`/svg/icn-${active ? `${icn}_white` : icn}.svg`} alt="" />
    </div>
  );
};

export default MenuIcon;
