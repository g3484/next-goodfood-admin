import React from 'react';
import styles from './TableHeader.module.scss';

type TableHeaderProps = {
  icn: string;
  children: React.ReactNode;
  onClick?: () => void;
};

const TableHeader = ({
  icn,
  onClick = () => {},
  children,
}: TableHeaderProps) => {
  return (
    <div className={styles.TableHeader}>
      <img src={`svg/icn-${icn}_white.svg`} onClick={onClick} />
      {children}
    </div>
  );
};

export default TableHeader;
