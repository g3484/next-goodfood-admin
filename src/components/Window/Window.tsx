import React from 'react';
import styles from './Window.module.scss';

type WindowProps = {
  children: React.ReactNode;
};

const Window = ({ children }: WindowProps) => {
  return <div className={styles.Window}>{children}</div>;
};

export default Window;
