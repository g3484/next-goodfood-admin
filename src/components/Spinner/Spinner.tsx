import React from 'react';
import ClipLoader from 'react-spinners/ClipLoader';
import styles from './Spinner.module.scss';

interface SpinnerProps {
  loading: boolean;
  color: string;
}

const Spinner = ({ loading, color }: SpinnerProps) => {
  return (
    <div className={styles.Spinner}>
      <ClipLoader color={color} loading={loading} size={150} />
    </div>
  );
};

export default Spinner;
