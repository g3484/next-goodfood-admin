import axiosClient from './axiosClient';

/** PRODUCT REQUEST */

export const getUsers = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const getRoles = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const updateUserRoles = async (URL: string, body: any) => {
  return await axiosClient
    .put(URL, body)
    .then((response) => response)
    .catch((error) => error.response);
};
