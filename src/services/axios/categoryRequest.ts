import axiosClient from './axiosClient';

/** PRODUCT REQUEST */

export const getCategories = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const getCategory = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const deleteCategory = async (URL: string) => {
  return await axiosClient
    .delete(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const updateCategory = async (URL: string, body: any) => {
  return await axiosClient
    .put(URL, body)
    .then((response) => response)
    .catch((error) => error.response);
};

export const createCategory = async (URL: string, body: any) => {
  return await axiosClient
    .post(URL, body)
    .then((response) => response)
    .catch((error) => error.response);
};
