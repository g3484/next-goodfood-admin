import axiosClient from './axiosClient';

/** PRODUCT REQUEST */

export const getProducts = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const getProduct = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const deleteProduct = async (URL: string) => {
  return await axiosClient
    .delete(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const updateProduct = async (URL: string, body: any) => {
  return await axiosClient
    .put(URL, body)
    .then((response) => response)
    .catch((error) => error.response);
};

export const createProduct = async (URL: string, body: any) => {
  return await axiosClient
    .post(URL, body)
    .then((response) => response)
    .catch((error) => error.response);
};
