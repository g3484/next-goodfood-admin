import React from 'react';
import Head from 'next/head';
import Layout from '../components/layout';
import Content from '../containers/Content/Content';

const Home = () => {
  return (
    <Layout>
      <Head>
        <title>Goodfood - Admin</title>
      </Head>
      <Content />
    </Layout>
  );
};

export default Home;
